
public class ToString {
	
	String nombre;
	String apellidos;
	
	ToString (String nombre,String apellidos){
		this.nombre = nombre;
		this.apellidos = apellidos;
	}
	
	@Override
	public String toString(){
		return this.nombre + " " + this.apellidos;
	}

	public static void main(String [] args){
		ToString t = new ToString("Alejandro","S�nchez");
		
		System.out.println(t.toString());
	}
	
}
