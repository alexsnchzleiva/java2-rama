
public class StringBufferClase {
	
	public static void main(String args[]){
		
		String cadena;
		cadena = "Esto es una cadena";
		System.out.println(cadena.valueOf(cadena));
		
		StringBuffer a = new StringBuffer(20);
		a.append("Hola Casa");
		System.out.println(a.length());
		System.out.println(a.charAt(5));
		System.out.println(a.capacity());
		a.setCharAt(4, '-');
		System.out.println(a);
		
	}

}
