/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package conexionjdbcoracle11g;

import java.sql.*;

/**
 *
 * @author alex
 */
public class ConexionJDBCOracle11G {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        // TODO code application logic here
        
        Connection conexion;
        Statement sentencia;
        ResultSet resultado;
        System.out.println("Iniciando programa");
        
        Class.forName("oracle.jdbc.OracleDriver").newInstance();
        conexion = DriverManager.getConnection("jdbc:oracle:oci:@//localhost:1521/XE","ADMIN","ADMIN");
        sentencia = conexion.createStatement();
        sentencia.execute("DROP TABLE ALUMNOS");
        sentencia.execute("CREATE TABLE ALUMNOS (NOMBRE VARCHAR(20))");
        sentencia.execute("INSERT INTO ALUMNOS NOMBRE VALUES('ALEJANDRO')");
        sentencia.execute("INSERT INTO ALUMNOS NOMBRE VALUES('JORGE')");
        sentencia.execute("INSERT INTO ALUMNOS NOMBRE VALUES('IVAN')");
        
        resultado = sentencia.executeQuery("SELECT NOMBRE FROM ALUMNOS");
        
        while(resultado.next()){
            String nombre = resultado.getString("NOMBRE");
            System.out.println(nombre);
        }
        
        System.out.println("Ejecución finalizada." + " AutoCommit: " + conexion.getAutoCommit());
        
        //DatabaseMetaDataClase dbmdc = new DatabaseMetaDataClase(conexion);
        //System.out.println("\nMostramos el nombre de las tablas:");
        //dbmdc.mostrarTablas();
        
    }
}
