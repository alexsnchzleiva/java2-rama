/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package conexionjdbcoracle11g;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author alex
 */
public class DatabaseMetaDataClase {
    
    String nombreTablas;
    String [] tipos;
    DatabaseMetaData dbmd;
    ResultSet tablas;
    boolean seguir;

    public DatabaseMetaDataClase(Connection conexion) throws SQLException {
        this.nombreTablas = "%";
        this.tipos = new String [1];
        this.tipos[0] = "TABLE";
        this.dbmd = conexion.getMetaData();
        this.tablas = dbmd.getTables(null, null, nombreTablas, tipos);
        this.seguir = tablas.next();
    }
    
    public void mostrarTablas() throws SQLException{
        while(this.seguir){
            System.out.println(tablas.getString(tablas.findColumn("TABLE_NAME")));
            this.seguir = tablas.next();
        }
    }
    
    
    
    
}
