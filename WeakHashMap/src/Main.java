
import java.util.WeakHashMap;


public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		WeakHashMap<String, String> whm = new WeakHashMap<String, String>();
		
		whm.put("Alex", "Alejandro");
		whm.put("Pepe", "Fernando");
		whm.put("Paco", "Francisco");
		
		System.out.println(whm.get("Alex"));

	}

}
