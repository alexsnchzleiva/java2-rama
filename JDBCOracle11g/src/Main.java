import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class Main {

	/**
	 * @param args
	 * @throws ClassNotFoundException 
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws ClassNotFoundException, SQLException {

		Connection conexion;
		Statement sentencia;
		ResultSet resultado;
		
		Class.forName("oracle.jdbc.OracleDriver");
		conexion = DriverManager.getConnection("jdbc:oracle:oci:@//localhost:1521/XE","ALEX","inlandempire");

		sentencia = conexion.createStatement();
        sentencia.execute("CREATE TABLE ALUMNOS (NOMBRE VARCHAR(20))");
        sentencia.execute("INSERT INTO ALUMNOS NOMBRE VALUES('ALEJANDRO')");
        sentencia.execute("INSERT INTO ALUMNOS NOMBRE VALUES('JORGE')");
        sentencia.execute("INSERT INTO ALUMNOS NOMBRE VALUES('IVAN')");
        
        resultado = sentencia.executeQuery("SELECT NOMBRE FROM ALUMNOS");
        
        while(resultado.next()){
            String nombre = resultado.getString("NOMBRE");
            System.out.println(nombre);
        }
        
        System.out.println("Ejecución finalizada." + " AutoCommit: " + conexion.getAutoCommit());
		
	}

}
