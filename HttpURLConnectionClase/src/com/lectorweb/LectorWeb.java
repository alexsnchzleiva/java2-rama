/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lectorweb;

import java.awt.HeadlessException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author alex
 */
public class LectorWeb extends JFrame {
    
    JTextArea caja = new JTextArea("Obteniendo datos ...");

    public LectorWeb() throws HeadlessException {
        super("Obtener Archivo Aplicación");
        super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        super.setSize(1280, 760);
        JScrollPane panel = new JScrollPane(caja);
        super.add(panel);
        super.setVisible(true);
    }
    
    public void obtenerDatos(String direccion) throws MalformedURLException, IOException{
        super.setTitle(direccion);
        
        URL pagina = new URL(direccion);
        StringBuffer texto = new StringBuffer();
        HttpURLConnection con = (HttpURLConnection)pagina.openConnection();
        con.connect();
        InputStreamReader in = new InputStreamReader((InputStream)con.getContent());
        BufferedReader buff = new BufferedReader(in);
        caja.setText("Obteniendo datos ...");
        String linea;
        do{
            linea = buff.readLine();
            texto.append(linea + "\n");
        }
        while(linea != null);
        caja.setText(texto.toString());
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws MalformedURLException, IOException {
        // TODO code application logic here
        LectorWeb lw = new LectorWeb();
        lw.obtenerDatos(args[0]);
    }
}
