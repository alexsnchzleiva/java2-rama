import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


public class Main {
	
	public void prueba(){
		ExecutorService es = Executors.newSingleThreadExecutor();
		System.out.println( "[1] Iniciando prueba en la primera tarea.." );
		
		try {
			Future <String> f = es.submit(new CallableImpl());
			Thread.sleep(1000);
			System.out.println( "[1] Primera tarea completada. Consultando a Future..");
			String salida = f.get();
			System.out.println( "El resultado desde Future es " +salida );
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		
		es.shutdown();
	}



	
	public static void main(String[] args) {

		Main m = new Main();
		m.prueba();
	}

}
