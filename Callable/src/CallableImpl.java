import java.util.concurrent.Callable;


public class CallableImpl implements Callable<String> {

	@Override
	public String call() throws Exception {
		System.out.println( "[2] Invocacion de call() en la segunda tarea.." );
		Thread.sleep(500);	
		
		System.out.println(
			      "[2] Completada la llamada a call() en la segunda tarea.." );
		return "Concluido";
	}

}
