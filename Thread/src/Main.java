
public class Main extends Thread {
	
	private String nombre;
	private int retardo;
	
	public Main(String nombre,int retardo){
		this.setNombre(nombre);
		this.setRetardo(retardo);
	}

	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public int getRetardo() {
		return retardo;
	}

	public void setRetardo(int retardo) {
		this.retardo = retardo;
	}
	
	@Override
	public void run(){
		try{
			Thread.sleep(this.getRetardo());
		}
		catch(InterruptedException ie){
			ie.printStackTrace();
		}
		
		System.out.println("Hola Mundo " + this.getNombre() + " " + this.getRetardo());
		
	}



	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Thread m1,m2,m3,m4;
		
		m1 = new Main("Alex",((int)(Math.random() * 3000)));
		m2 = new Main("Jorge",((int)(Math.random() * 3000)));
		m3 = new Main("Pedro",((int)(Math.random() * 3000)));
		m4 = new Main("Sergio",((int)(Math.random() * 3000)));
		
		System.out.println(m1.isAlive());
		
		m1.start();
		m2.start();
		m3.start();
		m4.start();
		
	}

}
