import java.util.ArrayList;
import java.util.Vector;


public class ForEach {

	public static void main (String [] args){
		
		Vector <Integer> vector = new Vector <Integer>();
		
		vector.addElement(1);
		vector.addElement(2);
		vector.addElement(3);
		
		for(Integer valor:vector){
			System.out.println(valor);
		}
		
		ArrayList <String> arrayList = new ArrayList<String>();
		
		arrayList.add("Lunes");
		arrayList.add("Martes");
		arrayList.add("Miercoles");
		arrayList.add("Jueves");
		arrayList.add("Viernes");
		
		for(String valor:arrayList){
			System.out.println(valor);
		}
		
	}
	
}
