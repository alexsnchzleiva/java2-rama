import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class FileReaderClase {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		int c;
		Integer [] texto;
		
		String rutaHome = System.getProperty("user.home");
		String fichero = "Ejemplo.txt";
		
		File file = new File(rutaHome,fichero);	
		FileReader fr = null;
		
		texto = new Integer[(int) file.length()];
		fr = new FileReader(file);
		
		int i=0;
		
		while((c = fr.read()) != -1){
			texto[i] = c;
			i++;
		}
		
		for(Integer n:texto){
			System.out.println((char)n.intValue());
		}

	}

}
