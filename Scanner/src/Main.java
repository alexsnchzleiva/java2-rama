import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;


public class Main {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {

		URL url;
		URLConnection conex;
		Scanner s;
		
		url = new URL("http://www.marca.com");
		conex = url.openConnection();
		s = new Scanner(conex.getInputStream());
		
		while(s.hasNext()){
			System.out.println(s.next());
		}
		
		File file = new File("text.txt");
		s = new Scanner(file);
		
		while(s.hasNext()){
			System.out.println(s.next());
		}

	}

}
