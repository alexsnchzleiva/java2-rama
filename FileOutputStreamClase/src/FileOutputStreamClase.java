import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


public class FileOutputStreamClase {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		String rutaHome = System.getProperty("user.home");
		String fichero = "Ejemplo.txt";
		
		File file = new File(rutaHome,fichero);	
		FileInputStream fis = new FileInputStream(file);
		FileOutputStream fos = new FileOutputStream(file);
		
		byte[] b = {65,66,67,68,69,70};
		fos.write(b);
		fos.close();
		
		byte [] bt = new byte[(int)file.length()]; 
		fis.read( bt );
		String cadena = new String( bt );

		System.out.println(cadena);

	}

}
