import java.util.TreeMap;


public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		TreeMap<Integer, String> tm = new TreeMap<Integer, String>();
		tm.put(1, "Primero");
		tm.put(2, "Segundo");
		tm.put(3, "Tercero");
		tm.put(4, "Cuarto");
		tm.put(5, "Quinto");
		
		System.out.println(tm.lowerKey(2));
		System.out.println(tm.pollLastEntry());
		System.out.println(tm.subMap(1, true, 2, true));
		System.out.println(tm.tailMap(2, true));
		
		
	}

}
