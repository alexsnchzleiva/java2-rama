
public class GetClass {
	
	String nombre;
	
	GetClass(){
		this.nombre = "Nombre por defecto";
	}
	
	GetClass(String nombre){
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		GetClass gc = new GetClass("Alex");
		
		System.out.println(gc.getClass().getFields());
		System.out.println(gc.getClass().getMethods());
		System.out.println(gc.getClass().getName());
		System.out.println(gc.getClass().hashCode());
		System.out.println(gc.getClass().getPackage());
		
		try {
			String a = gc.getClass().newInstance().getNombre();
			System.out.println(a);
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		

	}

}
