import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;


public class Main {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {

		File file = new File("texto.txt");
		file.createNewFile();
		
		RandomAccessFile raf = new RandomAccessFile(file, "rw");
		raf.seek(20);
		raf.writeUTF("Ejemplo r�pido de raf");
		
		raf.close();
		
		raf = new RandomAccessFile(file, "rw");
		String cadena = raf.readLine();
		
		System.out.println(cadena);
		raf.close();

	}

}
