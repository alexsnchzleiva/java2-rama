/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication4;

import java.sql.*;

/**
 *
 * @author Alex
 */
public class ConexionPostgreSQL {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        // TODO code application logic here
        
        Class.forName("org.postgresql.Driver");
        Connection conex = DriverManager.getConnection("jdbc:postgresql://localhost:5432/","postgres","inlandempire");
        Statement stmt = conex.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT nombre FROM alumnos");
        
        while (rs.next()){
            System.out.println(rs.getString("nombre"));
        }

        stmt.close();
        conex.close();
        
    }
}
