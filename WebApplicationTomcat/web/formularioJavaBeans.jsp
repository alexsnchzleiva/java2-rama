
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<jsp:useBean id="formBean" class="com.beans.NewBean"></jsp:useBean>
<jsp:setProperty name="formBean" property="*"></jsp:setProperty>

<!DOCTYPE html>

<html>
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Formulario con JavaBeans</title>
    </head>
    
    <body>
        
        <h1>Formulario</h1>
        
        <% if(request.getParameter("nombre") == null){ %>
        
        <h2>Informacion del usuario</h2>
        
        <form method="get" action="formularioJavaBeans.jsp">
            <p>Nombre <input type="text" name="nombre"></p>
            <p><input type="submit" value="Procesar"></p>
        </form>
        
        <% } else{
        %>
        <p>Su nombre es: <jsp:getProperty name="formBean" property="nombre"></jsp:getProperty></p>
        <a href="formularioJavaBeans.jsp">Vovler</a>
        <% } %>
        
    </body>
    
</html>

