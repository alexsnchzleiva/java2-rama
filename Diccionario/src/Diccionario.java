import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Set;


public class Diccionario {
	
	Hashtable <String,String> ht;
	
	
	public Diccionario(){
		ht = new Hashtable <String,String>();
	}
	
	public Hashtable<String, String> getHt() {
		return ht;
	}
	
	public void setHt(Hashtable<String, String> ht) {
		this.ht = ht;
	}

	
	public boolean anyadir(String key, String value){
		if(ht.containsKey(key)){
			System.out.println("Error al insertar");
			return false;
		}
		else{
			ht.put(key, value);
			System.out.println("Inserción correcta");
			return true;
		}
	}


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Diccionario d = new Diccionario();
		d.anyadir("Perro", "Dog");
		d.anyadir("Perro", "Dog");
		d.anyadir("Gato", "Cat");
		d.anyadir("Puerta", "Door");
		d.anyadir("Mesa", "Table");
		d.anyadir("Silla", "Chair");
		d.anyadir("Ventana", "Window");
		
		Set<String> a = d.getHt().keySet();
		
		System.out.println("..........................");
		
		for(String valor:a){
			System.out.println(valor);
		}
		
		Enumeration en = d.getHt().elements();
		
		System.out.println("..........................");
		
		while(en.hasMoreElements()){
			System.out.println(en.nextElement());
		}

		System.out.println("..........................");
		
		System.out.println(d.getHt().get("Gato"));
		
	}

}
