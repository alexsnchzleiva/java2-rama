import java.util.LinkedList;


public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		LinkedList <String> ll = new LinkedList<String>();
		ll.add("Primero");
		ll.add("Segundo");
		ll.add("Tercero");

		ll.addFirst("Cero");
		ll.addLast("Cuarto");
		
		ll.push("Cuenta: ");
		
		System.out.println(ll.toString());

	}

}
