import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		List<String> l = Collections.synchronizedList(new ArrayList<String>());
		
		Map<String,String> m = Collections.synchronizedMap(new TreeMap<String,String>());
		
		Set <String> s = Collections.synchronizedSet(new HashSet<String>());
	}

}
