
public class Main implements Runnable {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Thread t1 = new Thread(new Main(),"t1");
		Thread t2 = new Thread(new Main(),"t2");
		Thread t3 = new Thread(new Main(),"t3");
		Thread t4 = new Thread(new Main(),"t4");
		
		t1.start();
		t2.start();
		t3.start();
		t4.start();

		try {
			Thread.currentThread().sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(Thread.currentThread());
		
	}

	@Override
	public void run() {
		System.out.println(Thread.currentThread());
	}

}
