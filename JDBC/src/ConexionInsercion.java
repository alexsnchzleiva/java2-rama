import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class ConexionInsercion {

	/**
	 * @param args
	 * @throws ClassNotFoundException 
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws ClassNotFoundException, SQLException {

		Connection conexion;
		Statement sentencia;
		ResultSet resultado;
		
		Class.forName("com.mysql.jdbc.Driver");
		conexion = DriverManager.getConnection(
				"jdbc:mysql://localhost/new_schema?" + "user=root&password=inlandempire");
		sentencia = conexion.createStatement();
		sentencia.execute("DROP TABLE IF EXISTS LIBROS");
		sentencia.execute("CREATE TABLE LIBROS ("+
				"TITULO VARCHAR(50) NOT NULL , AUTOR VARCHAR(50) NOT NULL)");
		sentencia.execute("INSERT INTO LIBROS " + 
				"VALUES('MySQL' , 'SUN')");
		sentencia.execute("INSERT INTO LIBROS " + 
				"VALUES('PostgreSQL' , 'OPEN SOURCE')");
		sentencia.execute("INSERT INTO LIBROS " + 
				"VALUES('Oracle' , 'ORACLE')");
		sentencia.execute("INSERT INTO LIBROS " + 
				"VALUES('Acces' , 'Microsoft')");

	}

}
