import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class ConexionQuery {

	/**
	 * @param args
	 * @throws ClassNotFoundException 
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws ClassNotFoundException, SQLException {

		Connection conexion;
		Statement sentencia;
		ResultSet resultado;
		
		Class.forName("com.mysql.jdbc.Driver");
		conexion = DriverManager.getConnection(
				"jdbc:mysql://localhost/new_schema?" + "user=root&password=inlandempire");
		sentencia = conexion.createStatement();
		resultado = sentencia.executeQuery("SELECT * FROM LIBROS");
		ResultSet resultSetMetaData = resultado;
		
		while(resultado.next()){
			String nombre = resultado.getString("TITULO");
			String autor = resultado.getString("AUTOR");
			System.out.println(nombre + " " + autor);
		}
		

	}

}
