import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;


public class Main {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {

		File file = new File("texto.txt");
		FileInputStream fis = new FileInputStream(file);

		for(int i=0;i<file.length();i++){
			System.out.print((char)fis.read());
		}

	}

}
