import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class Main {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {

		File file = new File("texto.txt");
		FileReader fr = new FileReader(file);
		
		int i;
		
		while(	(i = fr.read()) != -1){
			System.out.print((char)i);
		}

	}

}
