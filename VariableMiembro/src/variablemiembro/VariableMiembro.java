/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package variablemiembro;

/**
 *
 * @author alex
 */
public class VariableMiembro {
    /**
     * Variable miembro
     */
    private String nombre;
    
    private VariableMiembro(String nombre){
        this.nombre = nombre;
    }
    
    public void verNombre (){
        /**
         * Variable local
         */
        String ver = "Vamos a ver el nombre";
        System.out.println(ver + " " + this.nombre);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        VariableMiembro vm = new VariableMiembro("Alejandro");
        vm.verNombre();
        
        
    }
}
