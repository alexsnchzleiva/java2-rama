import java.util.Stack;


public class Pila {
	
	public static void main(String [] args){
		Stack <String> pila = new Stack <String> ();
		
		pila.push("Lunes");
		pila.push("Martes");
		pila.push("M�ercoles");
		pila.addElement("Jueves");
		
		//System.out.println(pila.elementAt(0));
		
		while(!pila.empty()){
			System.out.println(pila.pop());
		}
		
	}

}
