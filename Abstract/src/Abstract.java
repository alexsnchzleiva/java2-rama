
public abstract class Abstract {
	
	private String nombre;

	
	
	
	protected Abstract(){}
	
	protected Abstract(String nombre){
		this.nombre = nombre;
	}
	
	
	
	
	
	protected String getNombre() {
		return nombre;
	}

	protected void setNombre(String nombre) {
		this.nombre = nombre;
	}


	
	
	protected void saluda(){
		System.out.println("Hola " + this.nombre);
	}
	
	protected void saluda(String nombre){
		System.out.println("Hola " + nombre);
	}
	
	protected abstract void despedida();

}
