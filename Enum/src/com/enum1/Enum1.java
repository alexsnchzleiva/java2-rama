/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enum1;

/**
 *
 * @author alex
 */
public class Enum1 {
    
    static public enum Estacion{
        Primavera,Verano,Otoño,Invierno
    };
    
    public static void main(String args[]){
        
        for(Estacion e:Estacion.values()){
            System.out.println("La estación es " + e + " y es la " + (e.ordinal() + 1));
        }        
        
    }
    
}
