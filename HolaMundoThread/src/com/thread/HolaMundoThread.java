package com.thread;

public class HolaMundoThread extends Thread{
	
	private String nombre;
	private int retardo;

	public HolaMundoThread(String nombre, int retardo) {
		super();
		this.nombre = nombre;
		this.retardo = retardo;
	}
	
	public void run(){
		try {
			super.sleep(retardo);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Hola Mundo " + this.nombre + " " +  this.retardo);
	}

	public static void main(String[] args) {
		
		HolaMundoThread t1,t2,t3;
		t1 = new HolaMundoThread("Thread 1",(int)(Math.random()*2000));
		t2 = new HolaMundoThread("Thread 2",(int)(Math.random()*2000));
		t3 = new HolaMundoThread("Thread 3",(int)(Math.random()*2000));
		
		t1.setPriority(1);
		t2.setPriority(5);
		t3.setPriority(10);
		
		t1.start();
		t2.start();
		t3.start();

	}

}
