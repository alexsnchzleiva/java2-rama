import java.util.Enumeration;
import java.util.Hashtable;
import java.util.TreeSet;


public class HashTableCollection {
	
	public static void main(String [] args){
		
		Hashtable <String,Integer> ht = new Hashtable <String,Integer>();
		ht.put("Primero", new Integer (1));
		ht.put("Segundo", new Integer(2));
		ht.put("Tercero", new Integer(3));
		
		System.out.println(ht.toString());
		Enumeration e = ht.elements();
		while(e.hasMoreElements())
			System.out.println(e.nextElement());
		System.out.println(ht.get("Primero"));
		
	}

}
