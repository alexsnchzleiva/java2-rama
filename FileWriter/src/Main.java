import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


public class Main {
	
	public static void main(String[] args) throws IOException{
		
		File file = new File("texto.txt");
		FileWriter fw = new FileWriter(file);
		
		fw.write("Esto es una prueba de la Clase FieWrite");
		
		CharSequence c = "jajja va bien sin el buffered.";
		fw.append(c);
		
		c = "Ejemplo de una l�nea.\n.";
		fw.append(c);

		c = "Ejemplo de una l�nea.\n.";
		fw.append(c);
		
		fw.close();
		
	}

}
