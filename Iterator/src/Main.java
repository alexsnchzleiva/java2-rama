import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;


public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Vector<Integer> v = new Vector<Integer>();
		v.add(1);
		v.add(2);
		v.add(3);
		v.add(4);
		v.add(5);
		
		if(v instanceof Collection){
			Collection<Integer> c = (Collection<Integer>)v;
			Iterator<Integer> it = c.iterator();
			
			while(it.hasNext()){
				System.out.println(it.next());
				it.remove();
			}
			
		}
		
	}

}
