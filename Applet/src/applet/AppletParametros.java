/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package applet;

import java.applet.Applet;
import java.awt.Graphics;

/**
 *
 * @author alex
 */
public class AppletParametros extends Applet {
    
    String nombre;

    /**
     * Initialization method that will be called after the applet is loaded into
     * the browser.
     */
    @Override
    public void init() {
        nombre = getParameter("NOMBRE");
    }
    
    @Override
    public void start(){}
    
    @Override
    public void stop(){}
    
    @Override
    public void destroy(){}
    
    @Override
    public void paint(Graphics g){
        g.drawString("Hola Mundo " + nombre, 70, 70);
    }
}
