import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


public class Main {
	
	public static void main(String[] args) throws IOException{
		
		File file = new File("texto.txt");
		FileWriter fw = new FileWriter(file);
		BufferedWriter bw = new BufferedWriter(fw);
		
		bw.write("Esto es una prueba de la Clase FieWrite");
		
		CharSequence c = "jajja va bien sin el buffered.";
		bw.append(c);
		
		c = "Ejemplo de una l�nea.\n.";
		bw.append(c);

		c = "Ejemplo de una l�nea.\n.";
		bw.append(c);
		
		bw.close();
		
	}

}
