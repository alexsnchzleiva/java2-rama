/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tiposgenericoscomodines;

import java.util.Vector;

/**
 *
 * @author alex
 */
public class TiposGenericosComodines {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Vector <Automovil> coches = new Vector<Automovil>();
        coches.add(new Automovil());
        coches.add(new Automovil());
        
        Vector <Motocicleta> motos = new Vector<Motocicleta>();
        motos.add(new Motocicleta());
        motos.add(new Motocicleta());
        
        Vector <? extends Vehiculo> misVehiculos = coches;
        
        for(Vehiculo v:misVehiculos){
            v.arrancar();
        }
        
        Vector<Motocicleta> misMotos = motos;
        
        for(Vehiculo v:misMotos){
            v.detener();
        }
        
    }
}
