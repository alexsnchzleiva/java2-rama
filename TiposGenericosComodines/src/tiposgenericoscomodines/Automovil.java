/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tiposgenericoscomodines;

/**
 *
 * @author alex
 */
public class Automovil extends Vehiculo {

    @Override
    public void arrancar() {
        System.out.println("El coche arranca...");
    }

    @Override
    public void detener() {
        System.out.println("El coche se detiene...");
    }
    
}
