/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tiposgenericoscomodines;

/**
 *
 * @author alex
 */
public class Motocicleta extends Vehiculo {
    
    @Override
    public void arrancar() {
        System.out.println("La moto arranca...");
    }

    @Override
    public void detener() {
        System.out.println("La moto se detiene...");
    }
    
}
