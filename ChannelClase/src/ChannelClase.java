import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;


public class ChannelClase {
	
	public static void main(String [] args) throws IOException{
		
		String rutaHome = System.getProperty("user.home");
		String fichero1 = "Ejemplo.txt";
		String fichero2 = "Ejemplo2.txt";
		
		File file1 = new File(rutaHome,fichero1);
		File file2 = new File(rutaHome,fichero2);
		
		FileChannel entrada;
		FileChannel salida;
		
		entrada = new FileInputStream(file1).getChannel();
		salida = new FileOutputStream(file2).getChannel();

		salida.transferFrom( entrada,0L,(int)entrada.size() );
		
		entrada.close();
		salida.close();

		
	}

}
