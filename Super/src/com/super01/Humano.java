package com.super01;

public class Humano {
	
	private String nombre;
	private String apellido;
	
	Humano(String nombre,String apellidos){
		this.nombre = nombre;
		this.apellido = apellidos;
	}
	
	protected void verNombre(){
		System.out.println(this.nombre + " " + this.apellido);
	}

}
