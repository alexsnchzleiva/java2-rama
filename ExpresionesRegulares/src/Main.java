import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String cadena = "alejandro.sanchezleiva@gmail.com";

		Pattern p = Pattern.compile("^\\.|^\\@");
		Matcher m = p.matcher(cadena);
		
		if(m.find())
			System.out.println("El mail no puede empezar por . o @");
		
		p = Pattern.compile("^www\\.|^http");
		m = p.matcher(cadena);
		
		if(m.find())
			System.out.println("No vale una direcc�n web");
		
		p = Pattern.compile("[^A-Za-z0-9\\.\\@_\\-~#]+");
		m = p.matcher(cadena);
		
		System.out.println("Validaci�n correcta");
		
	}

}

