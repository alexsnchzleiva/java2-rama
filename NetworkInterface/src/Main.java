
import java.io.*;
import java.net.*;
import java.util.*;

public class Main {
  private static final Console consola = System.console();

  public static void main( String args[] ) throws SocketException {
    Enumeration<NetworkInterface> redes =
      NetworkInterface.getNetworkInterfaces();
    for( NetworkInterface ni : Collections.list(redes) ) {
      displayInterfaceInformation( ni );
      }
    }

  private static void displayInterfaceInformation( NetworkInterface ni )
    throws SocketException {
    consola.printf( "Interfaz de Red: %s%n",ni.getDisplayName() );
    consola.printf( "Nombre: %s%n",ni.getName() );
    Enumeration<InetAddress> ia = ni.getInetAddresses();
    for( InetAddress inetAddress : Collections.list(ia) ) {
      consola.printf( "Direcci�n Inet: %s%n",inetAddress );
      }

    consola.printf( "Padre: %s%n",ni.getParent() );
    consola.printf( "Levantada: %s%n",ni.isUp() );
    consola.printf( "Bucle: %s%n",ni.isLoopback() );
    consola.printf( "Punto-a-punto: %s%n",ni.isPointToPoint() );
    consola.printf( "Soporte Multicast: %s%n",ni.supportsMulticast() );
    consola.printf( "Virtual: %s%n", ni.isVirtual() );
    consola.printf( "Direcci�n hardware: %s%n",
      Arrays.toString(ni.getHardwareAddress()) );
    consola.printf( "MTU: %s%n",ni.getMTU() );

    List<InterfaceAddress> lia = ni.getInterfaceAddresses();
    for( InterfaceAddress iad : lia ) {
      consola.printf( "Direcci�n Interfaz: %s%n",iad.getAddress() );
      }
    consola.printf( "%n" );
    Enumeration<NetworkInterface> subNi = ni.getSubInterfaces();
    for( NetworkInterface nif : Collections.list(subNi) ) {
      consola.printf( "%nSubInterfaz%n" );
      displayInterfaceInformation( nif );
      }
    consola.printf( "%n" );
    }
  }
