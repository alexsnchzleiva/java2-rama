import java.io.File;


public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		File file = new File("texto.txt");
		
		System.out.println(file.getAbsolutePath());
		System.out.println(file.getName());
		System.out.println(file.canRead());
		System.out.println(file.canWrite());
		System.out.println(file.length());
		
		System.out.println(file.setExecutable(true));
		System.out.println(file.setReadable(true));
		System.out.println(file.setWritable(true));

		System.out.println(file.canExecute());
		System.out.println(file.canRead());
		System.out.println(file.canWrite());

	}

}
