/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkgcontinue;

/**
 *
 * @author alex
 */
public class Continue {

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
        // TODO code application logic here
        
        for(int i=0;i<=10;i++){
            System.out.println("\n" + i);
            for(int j=65;j<75;j++){
                System.out.print((char)j);
                if(j==68)
                    continue;
            }
            System.out.println("");
        }
        
        System.out.println("-------------------------------------");
        
        etiqueta1 : for(int i=0;i<=10;i++){
            System.out.println("\n" + i);
            etiqueta2 : for(int j=65;j<75;j++){
                System.out.print((char)j);
                if(j==68)
                    continue etiqueta1;
            }
            System.out.println("");
        }
        
    }
}
