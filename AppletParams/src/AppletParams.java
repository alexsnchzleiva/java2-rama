import java.applet.Applet;
import java.awt.Graphics;

import javax.swing.JApplet;


public class AppletParams extends JApplet {
	
	String nombre;
	
	public void init(){
		nombre = this.getParameter("Nombre");
	}
	
	public void paint(Graphics g){
		g.drawString(nombre, 30, 30);
	}

}
