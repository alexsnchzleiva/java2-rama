import java.net.InetAddress;
import java.net.UnknownHostException;


public class Main {

	/**
	 * @param args
	 * @throws UnknownHostException 
	 */
	public static void main(String[] args) throws UnknownHostException {

		String host = "java.sun.com";
		
		InetAddress address = InetAddress.getByName(host);
		System.out.println(address);
	    address = InetAddress.getLocalHost();
		System.out.println(address);
		byte[] matriz = address.getAddress();
		
		for(int i=0;i<matriz.length;i++){
			int uByte = matriz[i] < 0 ? matriz[i] + 256: matriz[i];
			System.out.print(uByte);
			if(i==2 || i==1 || i==0)
				System.out.print(".");
		}
		
	}

}
