/**
 * 
 */
package com.referencia;

/**
 * @author alex
 *
 */


public class PasoPorReferencia {
	
	
	public PasoPorReferencia() {
	}
	
	public int sumarValor(int a){
		return ++a;
	}
	
	public Integer sumarReferencia(Integer b){
		return b.intValue() + 1;
	}

	public static void main(String [] args){
				int a = 4;
		Integer b = new Integer(8);
		
		PasoPorReferencia pr = new PasoPorReferencia();
	
		System.out.println(pr.sumarValor(a));
		System.out.println(a);
		
		System.out.println(pr.sumarReferencia(b.intValue()));
		System.out.println(b.intValue());
		
	}

}
