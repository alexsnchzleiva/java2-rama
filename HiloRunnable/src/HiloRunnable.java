
public class HiloRunnable {

	public static void main(String[] args) {
		Thread t1 = new Thread(new Hilo("Hilo 1"));
		Thread t2 = new Thread(new Hilo("Hilo 2"));
		Thread t3 = new Thread(new Hilo("Hilo 3"));
		Thread t4 = new Thread(new Hilo("Hilo 4"));
		Thread t5 = new Thread(new Hilo("Hilo 5"));
		Thread t6 = new Thread(new Hilo("Hilo 6"));
		
		t1.start();
		t2.start();
		t3.start();
		t4.start();
		t5.start();
		t6.start();
	}

}
