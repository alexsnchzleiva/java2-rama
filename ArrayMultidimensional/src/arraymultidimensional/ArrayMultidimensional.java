/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package arraymultidimensional;

/**
 *
 * @author alex
 */
public class ArrayMultidimensional {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        int isArray[][];
        isArray = new int[3][];
        isArray[0] = new int[1];
        isArray[1] = new int[2];
        isArray[2] = new int[3];
        
        for(int i=0;i<isArray.length;i++){            
            for(int j=0;j<isArray[i].length;j++){
                isArray[i][j] = j;
            }
        }
        
        for(int i=0;i<isArray.length;i++){
            for(int j=0;j<isArray[i].length;j++){
                System.out.print(isArray[i][j]);
            }
            System.out.println();
        }
        
    }
}
