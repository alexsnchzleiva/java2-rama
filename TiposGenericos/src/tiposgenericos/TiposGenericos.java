/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tiposgenericos;

/**
 *
 * @author alex
 */
public class TiposGenericos {
    
    public static class Clase<T1,T2>{
        private T1 tipo1;
        private T2 tipo2;

        public Clase(T1 tipo1, T2 tipo2) {
            this.tipo1 = tipo1;
            this.tipo2 = tipo2;
        }

        public T1 getTipo1() {
            return tipo1;
        }

        public T2 getTipo2() {
            return tipo2;
        }
        
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Clase <String,Integer> tG = new Clase<String,Integer>("Primero",1);
        
        String a = tG.getTipo1();
        Integer b = tG.getTipo2();
        
        System.out.println(a + " = " + b.intValue());
        
    }
}
