
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Runtime r = Runtime.getRuntime();
		System.out.println(r.freeMemory());
		System.out.println(r.totalMemory());
		System.out.println(r.maxMemory());
		r.gc();
		r.exit(0);

	}

}
