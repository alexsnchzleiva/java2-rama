import java.awt.Graphics;

import javax.swing.JApplet;


public class AppletEstructura extends JApplet {
	
	private String nombre;
	private String apellidos;
	
	public void init(){
		System.out.println("Init");
	}
	
	public void start(){
		System.out.println("Start");
	}
	
	public void paint(Graphics g){
		System.out.println("Paint");
		g.drawString("Nombre:", 10, 40);
	}
	
	public void stop(){
		System.out.println("Stop");
	}

	public void destroy(){
		System.out.println("Destroy");
	}

}
