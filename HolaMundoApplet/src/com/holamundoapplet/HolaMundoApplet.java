/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.holamundoapplet;

import java.applet.Applet;
import java.awt.Graphics;

/**
 *
 * @author alex
 */
public class HolaMundoApplet extends Applet {

    /**
     * Initialization method that will be called after the applet is loaded into
     * the browser.
     */
    public void init() {
        // TODO start asynchronous download of heavy resources
    }
    // TODO overwrite start(), stop() and destroy() methods
    
    public void paint(Graphics g){
        g.drawString("Hola Mundo", 25, 25);
    }
}
