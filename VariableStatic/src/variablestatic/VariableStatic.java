/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package variablestatic;

/**
 *
 * @author alex
 */
public class VariableStatic {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        AttrInstancia as1 = new AttrInstancia("Alejandro");
        System.out.println(AttrInstancia.numero);
        AttrInstancia as2 = new AttrInstancia("Ivan");
        System.out.println(AttrInstancia.numero);
        AttrInstancia as3 = new AttrInstancia("Jorge");
        System.out.println(AttrInstancia.numero);
        AttrInstancia as4 = new AttrInstancia("Pedro");
        System.out.println(AttrInstancia.numero);
        
    }
}
