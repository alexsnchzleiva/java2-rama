
public class Equals {
	
	int numero;
	
	Equals(int numero){
		this.numero = numero;
	}
	
	@Override
	public boolean equals(Object obj){
		if((obj != null) && (obj instanceof Equals)){
			Equals temp = (Equals)obj;
			return this.numero == temp.numero;
		}
		else return false;
		}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Equals e1 = new Equals(1);
		Equals e2 = new Equals(1);
		Equals e3 = new Equals(2);
		String e4 = "Hola";
		
		System.out.println(e1.equals(e1));
		System.out.println(e1.equals(e2));
		System.out.println(e1.equals(e3));
		System.out.println(e1.equals(e4));

	}

}
