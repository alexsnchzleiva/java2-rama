import java.util.Enumeration;
import java.util.Vector;


public class VectorColecciones {
	
	public static void main(String [] args){

		Vector <Integer> v = new Vector<Integer>();
		v.addElement(new Integer(1));
		v.addElement(new Integer(2));
		v.addElement(new Integer(3));
		v.addElement(new Integer(4));
		
		for(Integer valor:v){
			System.out.println(valor);
		}

	}

}
