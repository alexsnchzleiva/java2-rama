import java.util.Collection;
import java.util.LinkedHashMap;


public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		LinkedHashMap<String, String> lhm = new LinkedHashMap<String, String>();
		
		lhm.put("Gato", "Cat");
		lhm.put("Perro", "Dog");
		lhm.put("Caballo", "Horse");
		
		Collection<String> c = lhm.values();
		
		for(String valor:c){
			System.out.println(valor);
		}

	}

}
