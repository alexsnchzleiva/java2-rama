import java.util.Comparator;


public class Compare implements Comparator {

	@Override
	public int compare(Object arg0, Object arg1) {
		String s1 = ((String)arg0).toLowerCase();
		String s2 = ((String)arg1).toLowerCase();
		return s2.compareTo(s1);
	}

}
