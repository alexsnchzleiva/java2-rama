import java.applet.Applet;
import java.awt.Graphics;


public class Main extends Applet implements Runnable{
	
	Thread t;
	int contador;

	@Override
	public void init(){
		this.contador = 0;		
		t = new Thread(this);
		t.start();
	}
	
	@Override
	public void paint(Graphics g){
		g.drawString(Integer.toString(this.contador), 10, 10);
	}
	
	@Override
	public void stop(){
		this.t = null;
	}

	
	
	
	@Override
	public void run() {		
		Thread miThread = Thread.currentThread();
		
		while(t == miThread){
			this.contador++;
			this.repaint();
			
			try{
				miThread.sleep(500);
			}
			catch(InterruptedException ie){}
		}
	}
	

}


