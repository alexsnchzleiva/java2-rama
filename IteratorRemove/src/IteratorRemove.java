import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class IteratorRemove {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		List <String> lista = new ArrayList <String>();
		lista.add("L�nea 1");
		lista.add("L�nea 2");
		lista.add("L�nea 3");
		lista.add("L�nea 4");
		
		Iterator it = lista.iterator();
		
		for(int i=0;it.hasNext();i++){
			String elemento = (String)it.next();
			if(elemento.equals("L�nea 2")){
				it.remove();
			}
			System.out.println(elemento);
		}
		
		System.out.println("---------------------");
		Iterator it2 = lista.iterator();
		while(it2.hasNext()){
			System.out.println((String)it2.next());
		}

	}

}
