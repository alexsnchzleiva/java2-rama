import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;


public class FileInputStreamClase {

	public static void main(String [] args) throws IOException{
		
		String rutaHome = System.getProperty("user.home");
		String fichero = "Ejemplo.txt";
		
		File file = new File(rutaHome,fichero);	
		FileInputStream fis = new FileInputStream(file);
		
		byte [] bt = new byte[(int)file.length()]; 
		fis.read( bt );
		String cadena = new String( bt );

		System.out.println(cadena);
	
	}
}