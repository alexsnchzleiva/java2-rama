import java.io.File;


public class FileClase {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String ruta = "C:\\Users\\alex\\Documents\\Eclipse";
		String fichero = "Ejemplos.txt";
		
		File file = new File(ruta,fichero);
		
		System.out.println(file.setExecutable(true));
		System.out.println(file.setReadable(true));
		System.out.println(file.setWritable(true));
		
		System.out.println(file.getName());
		System.out.println(file.getPath());
		System.out.println(file.canRead());
		System.out.println(file.canWrite());
		System.out.println(file.length());
		
		
		
	}

}
