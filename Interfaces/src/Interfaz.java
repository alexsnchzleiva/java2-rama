
public interface Interfaz extends Aparato {
	
	public final double PI = 3.14;
	
	public void play();
	public void stop();
	public void eject();

}
