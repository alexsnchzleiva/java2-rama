import java.applet.Applet;
import java.awt.Graphics;


public class ExcepcionCapturaApplet extends Applet {

	private int i = 6;
	private String [] saludos = {"Hola Mundo","HOLA Mundo","HOLA MUNDO"};
	
	@Override
	public void paint(Graphics g){
		try{
		g.drawString(saludos[i], 25, 25);
		}
		catch(ArrayIndexOutOfBoundsException e){
			g.drawString("Saludo desbordado", 25, 25);
		}
		catch(Exception e){
			System.out.println(e.toString());
		}
		finally{
			System.out.println("Esto se imprime siempe");
		}
		
	}

}
