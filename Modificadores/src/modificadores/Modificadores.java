/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modificadores;

import com.abstracta.Abstracta;
import com.finall.Final;
import com.publico.Publico;
import com.syncronizable.Syncronizable;

/**
 *
 * @author alex
 */
public class Modificadores {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Publico publico = new Publico();
        
        Abstracta abstracta = new Abstracta() {
        };
        
        Final finall = new Final();
        
        Syncronizable s = new Syncronizable();
        
    }
}
