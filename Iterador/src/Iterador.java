import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;


public class Iterador {
	
	static void muestraElementos(Object obj){
		if(obj instanceof Map)
			obj = ((Map)obj).entrySet();
		if(obj instanceof Collection){
			Collection coleccion = (Collection)obj;
			Iterator it = coleccion.iterator();	
			while(it.hasNext()){
				System.out.println(it.next());
			}
		}
		else
			System.out.println("Esto no es una colecci�n");
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List <String> lista = new ArrayList<String>();
		lista.add("L�nea 1");
		lista.add("L�nea 2");
		muestraElementos(lista);
		
		Set<String>conjunto = new TreeSet<String>();
		conjunto.add("L�nea 3");
		conjunto.add("L�nea 4");
		muestraElementos(conjunto);
		
		Map <String,String> mapa = new HashMap <String,String>();
		mapa.put("L�nea 5: Clave", "L�nea 5: Valor");
		mapa.put("L�nea 6: Clave", "L�nea 6: Valor");
		muestraElementos(mapa);
		
	}

}
