import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Patron {
	
	public static void main(String args[]){
		
		String mail = "alejandro.sanchezleiva@me.com";

		Pattern p = Pattern.compile( "^\\.|^\\@" );
		Matcher m = p.matcher(mail);
		
		if(m.find())
			System.out.println	("El mail no puede empezar por puntos o signos");
		
		p = Pattern.compile( "^www\\." );
	    m = p.matcher(mail);
	    
	    if( m.find() )
	      System.out.println(
	        "S�lo las p�ginas web pueden empezar por \"www.\" " );
	    
	    
	    
	    p = Pattern.compile( "[^A-Za-z0-9\\.\\@_\\-~#]+" );
	    m = p.matcher(mail);
	    StringBuffer sb = new StringBuffer();
	    
	    boolean resultado = m.find();
	    boolean caracteresIlegales = false;

	    while(resultado) {
	      caracteresIlegales = true;
	      m.appendReplacement( sb,"" );
	      resultado = m.find();
	      }
	    m.appendTail( sb );
	    mail = sb.toString();
	    
	    if( caracteresIlegales )
	      System.out.println( "La direccion contiene caracteres ilegales" );
	    }

	    

		
	}


