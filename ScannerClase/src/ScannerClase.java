import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;


public class ScannerClase {
	
	public static void main(String [] args) throws IOException{
		
		
		URL url = new URL("http://www.nature-art.com");
		URLConnection conn = url.openConnection();
		Scanner sc = new Scanner(conn.getInputStream());
		sc.useDelimiter("\\z");
		System.out.println(sc.next());
		
		
	}

}
