import java.util.ArrayList;
import java.util.ListIterator;


public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		ArrayList <String> al = new ArrayList<String>();
		
		al.add("Primero");
		al.add("Segundo");
		al.add("Tercero");
		al.add(0, "Cero");
		al.add(4, "Cuarto");
		
		if(al.contains("Primero")){
			System.out.println("Existe un primero");
		}
		else{
			System.out.println("No existe");
		}
		
		
		//for(String valor:al){
		//	System.out.println(valor);
		//}
		
		
		ListIterator <String> li = al.listIterator();
	
		System.out.println(li.next());
		System.out.println(li.next());
		System.out.println(li.previous());
	

	}

}
