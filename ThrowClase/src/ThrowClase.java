
public class ThrowClase {
	
	static int [] slice = {1,2,3,4,5};
	
	static void uno(){
		try{
		slice[-1] = 4;
		}
		catch(NullPointerException e){
			System.out.println("Una excepci�n diferente");
		}
	}

	public static void main (String [] args){
		try{
			uno();
		}
		catch(Exception e){
			System.out.println("Captura en la excepci�n main");
			e.printStackTrace();
		}
	}
}
