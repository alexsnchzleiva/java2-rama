import java.util.PriorityQueue;


public class Main {
	
	public static void main(String[] args) {
		
		PriorityQueue<String> pq = new PriorityQueue<String>();
		pq.add("Jorge");
		pq.add("Alex");
		pq.add("Ram�n");
		
		pq.poll();
		
		
		System.out.println(pq.toString());
		
	}

}
