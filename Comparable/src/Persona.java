
import java.util.Set;
import java.util.TreeSet;


public class Persona implements Comparable <Persona> {
	
	String nombre;
	String apellidos;
	Integer edad;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}
	
	@Override
	public int compareTo(Persona o) {
		return o.getEdad().compareTo(this.getEdad());
	}



	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Persona p1 = new Persona();
		p1.setNombre("Alejandro");
		p1.setApellidos("S�nchez");
		p1.setEdad(31);
		Persona p2 = new Persona();
		p2.setNombre("Jorge");
		p2.setApellidos("Ant�n");
		p2.setEdad(28);
		Persona p3 = new Persona();
		p3.setNombre("Ivan");
		p3.setApellidos("Ram�n");
		p3.setEdad(22);
		Persona p4 = new Persona();
		p4.setNombre("Berto");
		p4.setApellidos("Mill�n");
		p4.setEdad(24);
		Persona p5 = new Persona();
		p5.setNombre("alvaro");
		p5.setApellidos("pombo");
		p5.setEdad(45);
		
		Set <Persona> l = new TreeSet<Persona>();
		l.add(p1);
		l.add(p2);
		l.add(p3);
		l.add(p4);
		l.add(p5);
		
		
		for(Persona p:l){
			System.out.print(p.getNombre());
			System.out.println(p.getEdad());
		}

	}


}
