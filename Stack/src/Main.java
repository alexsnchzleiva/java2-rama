import java.util.ListIterator;
import java.util.Stack;


public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String a = "Alejandro";
		String b = "Jorge";
		String c = "Pedro";
		String d = "Juan";
		String e = "Luis";
		
		Stack <String> s = new Stack <String> ();
		
		s.push(a);
		s.push(b);
		s.push(c);
		s.push(d);
		s.push(e);
		
		System.out.println(s.pop());
		System.out.println(s.pop());
		
		System.out.println(s.firstElement());
		
		ListIterator li = s.listIterator();
		
		System.out.println("-------------------");
		
		while(li.hasNext()){
			System.out.println(li.next());
		}
		

	}

}
