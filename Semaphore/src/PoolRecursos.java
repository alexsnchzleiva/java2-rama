import java.util.concurrent.Semaphore;


public class PoolRecursos {
	
	private int tamPool;
	private Semaphore semaforo;
	private Integer [] recursos;
	private boolean [] utilizados;
	
	public PoolRecursos(int tamPool){
		this.tamPool = tamPool;
		this.semaforo = new Semaphore(this.tamPool);
		this.recursos = new Integer[this.tamPool];	
		this.utilizados = new boolean[this.tamPool];
		
		for(int i=0;i<this.tamPool;i++){
			this.recursos[i] = new Integer(i);
		}
		
	}
	
	public synchronized Integer adquiereRecurso(){
		try {
			this.semaforo.acquire();
			
		} catch (InterruptedException e) {
			System.out.println(e.getLocalizedMessage());
		}
		
		for(int i=0;i<this.tamPool;i++){
			if(this.utilizados[i] == false){
				this.utilizados[i] = true;
				return this.recursos[i];
			}
		}
		
		return null;
	}
	
	public void liberaRecursos(Integer recurso){
		this.utilizados[recurso] = false;
		this.semaforo.release();
	}

}
