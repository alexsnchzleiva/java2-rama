import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class Main implements Runnable {

	private PoolRecursos pool;
	private int idTarea;
	private int contador;
	private long intervalo;
	private long pausa;
	
	public Main(PoolRecursos pool, int idTarea, int contador, long intervalo,
			long pausa) {
		super();
		this.pool = pool;
		this.idTarea = idTarea;
		this.contador = contador;
		this.intervalo = intervalo;
		this.pausa = pausa;
	}



	@Override
	public void run() {
		for(int i=0;i<contador;i++){
			try{
				System.out.printf("[%d] solicitando un recurso...", idTarea);
				Integer recurso = pool.adquiereRecurso();
				System.out.printf( "[%d] recurso adquirido.%n", idTarea );
				Thread.sleep(this.intervalo);
				System.out.printf( "[%d] liberando recurso..%n",idTarea );
				pool.liberaRecursos( recurso );
				Thread.sleep(this.pausa);
			}
			catch(InterruptedException ie){
				System.out.println( ie.getLocalizedMessage() );
			}
		}
	}
	
	public static void main(String [] args){
		PoolRecursos pool = new PoolRecursos(2);
		
		ExecutorService es = Executors.newFixedThreadPool( 3 );
		
		Main ej1 = new Main( pool,1,5,400,500 );
		Main ej2 = new Main( pool,2,5,400,500 );
	    Main ej3 = new Main( pool,3,5,400,500 );
	    es.execute( ej1 );
	    es.execute( ej2 );
	    es.execute( ej3 );

	    es.shutdown();
	}

}
