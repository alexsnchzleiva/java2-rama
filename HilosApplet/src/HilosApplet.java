import java.applet.Applet;
import java.awt.Graphics;


public class HilosApplet extends Applet implements Runnable {
	
	Thread t;
	int contador;
	
	public void init(){
		t = new Thread(this);
		ProcesoRaton pr = new ProcesoRaton(t);
		super.addMouseListener(pr);
		contador = 0;
		t.start();
	}

	@Override
	public void run() {
		Thread miThread = Thread.currentThread();
		
		while(t==miThread){
			this.contador++;
			super.repaint();
			try {
				miThread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	
	}
	
	public void paint(Graphics g){
		g.drawString(Integer.toString(contador), 25, 25);
		System.out.println("Contador: " + contador);
	}
	
	public void stop(){
		t = null;
	}

}
