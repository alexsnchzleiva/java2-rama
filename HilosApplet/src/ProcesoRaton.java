import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class ProcesoRaton extends MouseAdapter {
	
	Thread t;
	
	public ProcesoRaton(Thread t) {
		this.t = t;
	}

	public void MoussePressed(MouseEvent evt){
		this.t.stop();
	}

}
