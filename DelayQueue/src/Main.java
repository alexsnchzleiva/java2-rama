import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;


public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		DelayQueue dq = new DelayQueue();
		
		dq.add("Primero");
		dq.add("Segundo");
		dq.add("Tercero");
		
		dq.offer("Cuarto");
		
		long timeout = 10000;
		TimeUnit unit = TimeUnit.SECONDS;
		Delayed e = null;
		
		dq.offer(e, timeout, unit);

	}

}
