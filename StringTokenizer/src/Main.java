import java.util.StringTokenizer;


public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
			String cadena = "Hola Alejandro que tal est�s? Yo bien y tu";
			
			StringTokenizer st = new StringTokenizer(cadena);
			
			while(st.hasMoreTokens()){
				System.out.println(st.nextToken());
			}
			
			System.out.println();
			
			cadena = "<html><body></body></html>";
			
			st = new StringTokenizer(cadena,"</>");
			
			while(st.hasMoreTokens()){
				System.out.println(st.nextToken());
			}

	}

}
