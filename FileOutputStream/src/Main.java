import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


public class Main {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {

		File file = new File("texto.txt");
		FileOutputStream fos = new FileOutputStream(file);
		
		byte[] b = {65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90
				,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108};

		fos.write(b);
		fos.close();
		
	}

}
