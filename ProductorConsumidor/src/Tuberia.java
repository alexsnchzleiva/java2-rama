
public class Tuberia extends Thread {
	
	private char [] buffer = new char[6];
	private int siguiente = 0;
	private boolean estaLlena = false;
	private boolean estaVacia = true;
	
	public synchronized char recuperar(){
		while(estaVacia == true){
			try {
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		this.siguiente--;
		
		if(siguiente==0){
			this.estaVacia = true;
		}
		this.estaLlena = false;
		this.notify();
		
		return (buffer[this.siguiente]);
	}
	
	public synchronized void lanzar(char c) {
		while(this.estaLlena == true){
			try {
				this.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		buffer[siguiente] = c;
		siguiente++;
		
		if(siguiente == 6){
			this.estaLlena = true;
		}
		this.estaVacia = false;
		this.notify();
	}

}
