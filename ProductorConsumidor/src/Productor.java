
public class Productor extends Thread {
	
	private Tuberia tuberia;
	private String alfabeto = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
	public Productor(Tuberia t){
		this.tuberia = t;
	}
	
	@Override
	public void run(){
		char c;
		
		for(int i=0;i<10;i++){
			c = alfabeto.charAt((int)Math.random() * 26);
			tuberia.lanzar(c);
			
			System.out.println("Lanzado " + c + "a la tuber�a.");
			
			try {
				this.sleep(((int) Math.random() * 1000));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}

}
