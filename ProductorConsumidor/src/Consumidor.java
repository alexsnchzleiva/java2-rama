
public class Consumidor extends Thread{
	
	private Tuberia tuberia;
	
	public Consumidor(Tuberia t){
		this.tuberia = t;
	}
	
	@Override
	public void run(){
		char c;
		
		for(int i=0;i<10;i++){
			c = tuberia.recuperar();
			
			System.out.println("Recogido el caracter " + c);
			
			try {
				this.sleep((int)(Math.random() * 2000));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
