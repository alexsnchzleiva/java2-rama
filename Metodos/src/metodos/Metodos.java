/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package metodos;

/**
 *
 * @author alex
 */
public class Metodos {
    
    String nombre;
    static int numero = 0;

    public Metodos(String nombre) {
        this.nombre = nombre;
        ++this.numero;
    }
    
    public static void verVersion(){
        System.out.println(numero);
    }
    
    //public abstract void verAbstracto();
    
    public native void verNative();
    
    public synchronized void verHilo(){
    }
    
    
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Metodos.verVersion();
        Metodos m = new Metodos("Alejandro");
        Metodos.verVersion();
        
    }
}
