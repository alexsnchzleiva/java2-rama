import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


public class Main {
	
	public static void main(String [] args){
		
		String [] cadena = {"Primero","Segundo","Tercero"};
		
		List<String> lista = Arrays.asList(cadena);
		
		for(Iterator<String> it = lista.iterator();it.hasNext();){
			System.out.println(it.next());
		}
		
	}

}
