import java.util.Enumeration;
import java.util.Vector;


public class Main {


	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Vector <Main> v = new Vector<Main>();
		
		for(int i=0;i<10;i++){
			v.addElement(new Main());
		}
		
		for(Main valor:v){
			System.out.println(valor.toString());
		}

		System.out.println(v.elementAt(2));
		
		System.out.println("-----------------------");
		
		v.removeElementAt(0);
		v.removeElementAt(0);
		v.removeElementAt(0);
		v.removeElementAt(0);
		v.removeElementAt(0);
		v.removeElementAt(0);
		v.removeElementAt(0);
		
		
		Enumeration<Main> e = v.elements();
		
		while(e.hasMoreElements()){
			System.out.println(e.nextElement());
		}
		
	}

}
