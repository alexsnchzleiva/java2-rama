import java.util.HashMap;


public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("Gato", "Cat");
		hm.put("Perro", "Dog");
		hm.put("Caballo", "Horse");
		
		hm.remove("Caballo");
		
		if(hm.containsKey("Gato")){
			System.out.println(hm.get("Gato"));
		}
	}

}
