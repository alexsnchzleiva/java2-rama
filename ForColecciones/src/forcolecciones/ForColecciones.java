/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package forcolecciones;

/**
 *
 * @author alex
 */
public class ForColecciones {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        int a[];
        a = new int[5];
        
        a[0] = 10;
        a[1] = 20;
        a[2] = 30;
        a[3] = 40;
        a[4] = 50;
        
        for(int valor:a){
            System.out.println(valor);
        }
        
    }
}
